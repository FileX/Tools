#!/bin/bash

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Prerequesites:
# - Have Windows installed
# - use grub as bootloader
# - follow this until "sudo update-grub": https://askubuntu.com/a/1202887
# - be on linux

windows_title=$(sudo grep -i windows /boot/grub/grub.cfg | cut -d "'" -f 2)

sudo grub-reboot "$windows_title" && sudo grub-editenv - set boot_once_timeout=0 && sudo reboot
