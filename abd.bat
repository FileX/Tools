@echo off
echo Make sure to read the latest commit details before using
echo:

echo Making sure Discord is started... & echo Making sure Discord is started... > abd.log 
start /wait %LOCALAPPDATA%\DiscordCanary\Update.exe --processStart DiscordCanary.exe 

:STARTLOOP
(tasklist | find /I "DiscordCanary.exe" >nul 2>&1 && echo Shortly waiting so updates can finish... && timeout /t 5 >nul 2>&1 && GOTO INJECT) || ( 
    timeout /t 1 >nul 2>&1 
    GOTO STARTLOOP 
)

:INJECT
echo Checking BetterDiscord... && FOR /F "tokens=* USEBACKQ" %%F IN (`dir %USERPROFILE%\Downloads\BetterDiscord* /AD /B /s`) DO (
    echo BetterDiscord found at %%F
    echo BetterDiscord found at %%F >> abd.log
    cd %%F
    echo Injecting... >> abd.log
    echo Injecting... & pnpm inject canary >> abd.log 

    :INJECTCONT
    taskkill /im "DiscordCanary.exe" >> abd.log
    :KILLLOOP
    tasklist | find /I "DiscordCanary.exe" >nul 2>&1 && (GOTO KILLOOP) || ( 
        echo Starting Discord Canary >> abd.log & echo Starting Discord Canary & start /wait %LOCALAPPDATA%\DiscordCanary\Update.exe --processStart DiscordCanary.exe >> abd.log & GOTO KILLCONT
    )
    :KILLCONT
    exit /B
) || GOTO ERROR

echo BetterDiscord not found or blocked & echo BetterDiscord not found or blocked >> abd.log & echo: & echo Make sure to read the latest commit details before trying again & echo: & echo Logfile saved to ./abd.log & PAUSE & exit /B

:ERROR
echo There was an error. Maybe try again & echo An error occured >> abd.log & echo: & echo Logfile saved to ./abd.log & PAUSE


